package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true){
            System.out.println("Let's play round " + roundCounter);

            round();

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String inp = readInput("Do you wish to continue playing? (y/n)?");
            if(inp.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
        }
    }

    public void round(){
        String user = readInput("Your choice (Rock/Paper/Scissors)?");
        String cpu = computerInput();
        String winner = null;

        if(!rpsChoices.contains(user)){
            System.out.println("I do not understand " + user + ". Could you try again?");
            round();
            return;
        }

        if(user.equals(cpu)){

        } else if (user.equals("rock") && cpu.equals("scissors")){
            winner = "Human";
            humanScore++;
        } else if (user.equals("paper") && cpu.equals("rock")){
            winner = "Human";
            humanScore++;
        } else if (user.equals("scissors") && cpu.equals("paper")){
            winner = "Human";
            humanScore++;
        } else {
            winner = "Computer";
            computerScore++;
        }

        String sentence = "Human chose " + user + ", computer chose " + cpu + ". ";
        String result = (winner != null) ? winner + " wins!" : "It's a tie!";
        System.out.println(sentence + result);
    }

    /**
     * Get random pick from computer.
     * @return rock, paper or scissors.
     */
    public String computerInput(){
        int rand = new Random().nextInt(rpsChoices.size());
        return rpsChoices.get(rand);
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
